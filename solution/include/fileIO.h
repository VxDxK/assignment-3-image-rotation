#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILEIO_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILEIO_H
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

bool open_file(FILE** file, char const *filename, char const *open_mode);
bool close_file(FILE* file);
#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILEIO_H
