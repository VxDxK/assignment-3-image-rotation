#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGEIO_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGEIO_H
#include "image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};
enum  write_status  {
    WRITE_OK = READ_INVALID_HEADER + 1,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image ** img);
enum write_status to_bmp(FILE* out, struct image * img);
bool read_header(FILE *in, struct bmp_header * header);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGEIO_H
