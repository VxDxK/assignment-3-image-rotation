#include "image.h"

struct image* create_image(uint64_t width, uint64_t height) {
    struct pixel *pixels = malloc(sizeof (struct pixel) * (width * height));
    struct image *img = malloc(sizeof (struct image));
    img->width = width;
    img->height = height;
    img->data = pixels;
    return img;
}

void free_image(struct image * const image) {
    free(image->data);
    free(image);
}

struct image *rotate(struct image const * const image) {
    struct image* rotated = create_image(image->height, image->width);
    size_t c = 0;
    for (size_t i = 0; i < image->width; ++i) {
        for (size_t j = 0; j < image->height; ++j) {
            size_t index = i + (image->height - j - 1) * image->width;
            struct pixel px = image->data[index];
            rotated->data[c] = px;
            c++;
        }
    }
    return rotated;
}

int64_t calc_padding(size_t width){
    if(width % 4 == 0){
        return 0;
    }
    return 4 - (int64_t )((width * sizeof(struct pixel)) % 4);
}

size_t image_size(const struct image * const image){
    return (image->width + calc_padding(image->width)) * image->height;
}

size_t file_size(const struct image * const image){
    return image->width * image->height * sizeof(struct pixel) + calc_padding(image->width) * image->height + sizeof(struct bmp_header);
}

struct bmp_header generate_header(struct image const * const image) {
    return (struct bmp_header) {
            .bfType = 0x4d42,
            .bfileSize = file_size(image),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size(image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

}
