#include "imageIO.h"

bool read_header(FILE* const in, struct bmp_header * const header){
    size_t t = fread(header, sizeof(struct bmp_header), 1, in);
    return t;
}

enum read_status from_bmp(FILE* const in, struct image** const img) {
    struct bmp_header header = {0};
    if(!read_header(in, &header)){
        return READ_INVALID_HEADER;
    }
    *img = create_image(header.biWidth, header.biHeight);

    int64_t padding = calc_padding(header.biWidth);
    size_t c = 0;
    for (size_t i = 0; i < header.biHeight; ++i) {
        for (size_t j = 0; j < header.biWidth; ++j) {
            if(!fread(((*img)->data + c), sizeof (struct pixel), 1, in)){
                free_image(*img);
                return READ_INVALID_BITS;
            }
            c++;
        }
        if(fseek(in, padding, SEEK_CUR)){
            free_image(*img);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* const out, struct image* const img) {
    struct bmp_header header = generate_header(img);
    if(!fwrite(&header, sizeof (struct bmp_header), 1, out)){
        return WRITE_HEADER_ERROR;
    }
    if(fseek(out, header.bOffBits, SEEK_SET)){
        return WRITE_ERROR;
    }

    uint8_t tmp_bytes[3] = {0, 0, 0};
    size_t padding = calc_padding(img->width);
    size_t c = 0;
    for (size_t i = 0; i < img->height; ++i) {
        struct pixel *to_write = ((*img).data + c);
        size_t writed = fwrite(to_write, sizeof (struct pixel), img->width, out);
        if(writed < img->width){
            return WRITE_ERROR;
        }
        c += img->width;
        if(padding != 0){
            size_t writed_padding = fwrite(&tmp_bytes, sizeof (uint8_t), padding, out);
            if(writed_padding < padding){
                return WRITE_ERROR;
            }
        }

    }
    return WRITE_OK;
}
