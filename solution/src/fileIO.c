#include "fileIO.h"

bool open_file(FILE** const file, char const * const filename, char const * const open_mode) {
    *file = fopen(filename, open_mode);
    return *file != NULL;
}


bool close_file(FILE * const file) {
    return fclose(file) == 0;
}
